package net.rieksen.networkessentials.spigot.command;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.rieksen.networkessentials.spigot.NetworkEssentials;

public class FlyCommand extends NetworkEssentialsCommand
{

	public FlyCommand(NetworkEssentials plugin)
	{
		super(plugin, "fly", "networkessentials.fly");
	}

	@Override
	public void doCommand(CommandSender sender, Command cmd, String commandLabel, String[] args)
	{
		if (args.length == 0)
		{
			if (!(sender instanceof Player))
			{
				String message = this.getMessage("General", "player-only-cmd", sender);
				this.sendColoredMessage(sender, message);
				return;
			}
			Player player = (Player) sender;
			boolean state = this.toggleFlymode(player);
			String message = this.getMessage("Fly Command", "fly-self", sender).replace("%state%", Boolean.toString(state));
			this.sendColoredMessage(sender, message);
			return;
		}
		if (!(sender.hasPermission("networkessentials.fly.other")))
		{
			String message = this.getMessage("General", "no-permission", sender);
			this.sendColoredMessage(sender, message);
			return;
		}
		String playerName = args[0];
		Player player = this.plugin.getServer().getPlayer(playerName);
		if (player == null)
		{
			this.sendColoredMessage(sender, this.getMessage("Fly Command", "player-not-found", sender).replace("%player%", playerName));
			return;
		}
		boolean state = this.toggleFlymode(player);
		this.sendColoredMessage(sender, this.getMessage("Fly Command", "fly-other", sender).replace("%player%", player.getName())
			.replace("%state%", Boolean.toString(state)));
		this.sendColoredMessage(player, this.getMessage("Fly Command", "fly-by-other", player).replace("%by%", sender.getName())
			.replace("%state%", Boolean.toString(state)));
	}

	@Override
	public List<String> doTabComplete(CommandSender sender, Command cmd, String commandLabel, String[] args)
	{
		if (args.length == 1)
		{
			List<String> list = new ArrayList<>();
			String playerName = args[0].toLowerCase();
			this.plugin.getServer().getOnlinePlayers().stream().filter(player -> player.getName().toLowerCase().startsWith(playerName))
				.forEach(player -> list.add(player.getName()));
			return list;
		}
		return null;
	}

	private boolean toggleFlymode(Player player)
	{
		boolean state = !player.getAllowFlight();
		player.setAllowFlight(state);
		player.setFlying(state);
		return state;
	}
}
