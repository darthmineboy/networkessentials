package net.rieksen.networkessentials.spigot.command;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.rieksen.networkessentials.spigot.NetworkEssentials;

public class FeedCommand extends NetworkEssentialsCommand
{

	public FeedCommand(NetworkEssentials plugin)
	{
		super(plugin, "heal", "networkessentials.feed");
	}

	@Override
	public void doCommand(CommandSender sender, Command cmd, String commandLabel, String[] args)
	{
		if (args.length == 0)
		{
			if (!(sender instanceof Player))
			{
				String message = this.getMessage("General", "player-only-cmd", sender);
				this.sendColoredMessage(sender, message);
				return;
			}
			Player player = (Player) sender;
			this.feedPlayer(player);
			String message = this.getMessage("Feed Command", "feed-self", sender);
			this.sendColoredMessage(sender, message);
			return;
		}
		if (!(sender.hasPermission("networkessentials.feed.other")))
		{
			String message = this.getMessage("General", "no-permission", sender);
			this.sendColoredMessage(sender, message);
			return;
		}
		String playerName = args[0];
		Player player = this.plugin.getServer().getPlayer(playerName);
		if (player == null)
		{
			this.sendColoredMessage(sender, this.getMessage("Feed Command", "player-not-found", sender).replace("%player%", playerName));
			return;
		}
		this.feedPlayer(player);
		this.sendColoredMessage(sender, this.getMessage("Feed Command", "feed-other", sender).replace("%player%", player.getName()));
		this.sendColoredMessage(player, this.getMessage("Feed Command", "feed-by-other", player).replace("%by%", sender.getName()));
	}

	@Override
	public List<String> doTabComplete(CommandSender sender, Command cmd, String commandLabel, String[] args)
	{
		if (args.length == 1)
		{
			List<String> list = new ArrayList<>();
			String playerName = args[0].toLowerCase();
			this.plugin.getServer().getOnlinePlayers().stream().filter(player -> player.getName().toLowerCase().startsWith(playerName))
				.forEach(player -> list.add(player.getName()));
			return list;
		}
		return null;
	}

	private void feedPlayer(Player player)
	{
		player.setFoodLevel(20);
	}
}
