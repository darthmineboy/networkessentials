package net.rieksen.networkessentials.spigot.command;

import net.rieksen.networkcore.core.plugin.INetworkPlugin;
import net.rieksen.networkcore.spigot.command.NetworkPluginCommand;
import net.rieksen.networkessentials.spigot.NetworkEssentials;

public abstract class NetworkEssentialsCommand extends NetworkPluginCommand
{

	protected NetworkEssentials plugin;

	public NetworkEssentialsCommand(NetworkEssentials plugin, String commandName, String permission)
	{
		super(commandName, permission);
		this.plugin = plugin;
	}

	@Override
	public INetworkPlugin getNetworkPlugin()
	{
		return this.plugin.getNetworkPlugin();
	}
}
