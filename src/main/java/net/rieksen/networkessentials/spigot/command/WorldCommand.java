package net.rieksen.networkessentials.spigot.command;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.md_5.bungee.api.ChatColor;
import net.rieksen.networkessentials.spigot.NetworkEssentials;

public class WorldCommand extends NetworkEssentialsCommand
{

	public WorldCommand(NetworkEssentials plugin)
	{
		super(plugin, "world", "networkessentials.world");
	}

	@Override
	public void doCommand(CommandSender sender, Command cmd, String commandLabel, String[] args)
	{
		if (!(sender instanceof Player))
		{
			String message = this.getMessage("General", "player-only-cmd", sender);
			sender.sendMessage(ChatColor.translateAlternateColorCodes('&', message));
			return;
		}
		if (args.length < 1)
		{
			String message = this.getMessage("World Command", "usage", sender);
			sender.sendMessage(ChatColor.translateAlternateColorCodes('&', message));
			return;
		}
		String worldName = args[0];
		World world = this.plugin.getServer().getWorld(worldName);
		if (world == null)
		{
			String message = this.getMessage("World Command", "world-not-found", sender).replace("%world%", worldName);
			sender.sendMessage(ChatColor.translateAlternateColorCodes('&', message));
			return;
		}
		Player player = (Player) sender;
		player.teleport(world.getSpawnLocation());
	}

	@Override
	public List<String> doTabComplete(CommandSender sender, Command cmd, String commandLabel, String[] args)
	{
		if (args.length == 1)
		{
			String worldName = args[0].toLowerCase();
			List<String> list = new ArrayList<>();
			this.plugin.getServer().getWorlds().stream().filter(world -> world.getName().toLowerCase().startsWith(worldName))
				.forEach(world -> list.add(world.getName()));;
			return list;
		}
		return null;
	}
}
