package net.rieksen.networkessentials.spigot.command;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.rieksen.networkessentials.spigot.NetworkEssentials;
import net.rieksen.networkessentials.spigot.gui.GamemodeGUI;

public class GamemodeCommand extends NetworkEssentialsCommand
{

	public GamemodeCommand(NetworkEssentials plugin)
	{
		super(plugin, "gamemode", "networkessentials.gamemode");
	}

	@Override
	public void doCommand(CommandSender sender, Command cmd, String commandLabel, String[] args)
	{
		if (args.length == 0)
		{
			// Open gamemode menu
			if (!(sender instanceof Player))
			{
				this.sendColoredMessage(sender, this.getMessage("General", "player-only-cmd", sender));
				return;
			}
			Player player = (Player) sender;
			GamemodeGUI gui = new GamemodeGUI(this.plugin, player);
			gui.open(player);
		} else if (args.length == 1)
		{
			// Switch gamemode
			if (!(sender instanceof Player))
			{
				this.sendColoredMessage(sender, this.getMessage("General", "player-only-cmd", sender));
				return;
			}
			String gamemodeName = args[0];
			GameMode gamemode = this.getGameMode(gamemodeName);
			if (gamemode == null)
			{
				this.sendColoredMessage(sender,
					this.getMessage("Gamemode Command", "gamemode-not-found", sender).replace("%gamemode%", gamemodeName));
				return;
			}
			Player player = (Player) sender;
			player.setGameMode(gamemode);
			this.sendColoredMessage(sender,
				this.getMessage("Gamemode Command", "gamemode-changed", sender).replace("%gamemode%", gamemode.name().toLowerCase()));
		} else
		{
			// Switch gamemode for player
			if (!(sender.hasPermission("networkessentials.gamemode.other")))
			{
				String message = this.getMessage("General", "no-permission", sender);
				this.sendColoredMessage(sender, message);
				return;
			}
			String playerName = args[0];
			Player player = this.plugin.getServer().getPlayer(playerName);
			if (player == null)
			{
				this.sendColoredMessage(sender,
					this.getMessage("Gamemode Command", "player-not-found", sender).replace("%player%", playerName));
				return;
			}
			String gamemodeName = args[1];
			GameMode gamemode = this.getGameMode(gamemodeName);
			if (gamemode == null)
			{
				this.sendColoredMessage(sender,
					this.getMessage("Gamemode Command", "gamemode-not-found", sender).replace("%gamemode%", gamemodeName));
				return;
			}
			player.setGameMode(gamemode);
			this.sendColoredMessage(sender, this.getMessage("Gamemode Command", "gamemode-changed-other", sender)
				.replace("%gamemode%", gamemode.name().toLowerCase()).replace("%player%", player.getName()));
			this.sendColoredMessage(player, this.getMessage("Gamemode Command", "gamemode-changed-by-other", player)
				.replace("%gamemode%", gamemode.name().toLowerCase()).replace("%by%", sender.getName()));
		}
	}

	@Override
	public List<String> doTabComplete(CommandSender sender, Command cmd, String commandLabel, String[] args)
	{
		if (args.length == 1)
		{
			List<String> list = new ArrayList<>();
			String arg = args[0];
			Arrays.asList(GameMode.values()).stream().filter(gamemode -> gamemode.name().toLowerCase().startsWith(arg))
				.forEach(gamemode -> list.add(gamemode.name().toLowerCase()));
			this.plugin.getServer().getOnlinePlayers().stream().filter(player -> player.getName().toLowerCase().startsWith(arg))
				.forEach(player -> list.add(player.getName()));
			return list;
		} else if (args.length == 2)
		{
			List<String> list = new ArrayList<>();
			String gamemodeName = args[1];
			Arrays.asList(GameMode.values()).stream().filter(gamemode -> gamemode.name().toLowerCase().startsWith(gamemodeName))
				.forEach(gamemode -> list.add(gamemode.name().toLowerCase()));
			return list;
		}
		return null;
	}

	private GameMode getGameMode(String gamemodeName)
	{
		for (GameMode gamemode : GameMode.values())
		{
			if (gamemode.name().toLowerCase().startsWith(gamemodeName.toLowerCase())) { return gamemode; }
		}
		return null;
	}
}
