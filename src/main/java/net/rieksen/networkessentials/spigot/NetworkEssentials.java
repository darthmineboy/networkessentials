package net.rieksen.networkessentials.spigot;

import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import net.rieksen.networkcore.core.NetworkCoreAPI;
import net.rieksen.networkcore.core.importer.JSONImporter;
import net.rieksen.networkcore.core.plugin.INetworkPlugin;
import net.rieksen.networkessentials.spigot.command.FeedCommand;
import net.rieksen.networkessentials.spigot.command.FlyCommand;
import net.rieksen.networkessentials.spigot.command.GamemodeCommand;
import net.rieksen.networkessentials.spigot.command.HealCommand;
import net.rieksen.networkessentials.spigot.command.WorldCommand;

public class NetworkEssentials extends JavaPlugin
{

	private INetworkPlugin networkPlugin;

	public INetworkPlugin getNetworkPlugin()
	{
		return this.networkPlugin;
	}

	@Override
	public void onDisable()
	{}

	@Override
	public void onEnable()
	{
		this.validateNetworkCoreDependency();
		this.setupNetworkPlugin();
		this.importDefaults();
		this.registerCommands();
	}

	private void importDefaults()
	{
		JSONImporter populator = JSONImporter.loadInputStream(NetworkCoreAPI.getProvider(), this.getResource("import.json"));
		populator.importData();
	}

	private void registerCommands()
	{
		GamemodeCommand gamemode = new GamemodeCommand(this);
		gamemode.addAlias("gm");
		gamemode.registerCommand("gamemode", this.getName());
		HealCommand heal = new HealCommand(this);
		heal.registerCommand("heal", this.getName());
		FeedCommand feed = new FeedCommand(this);
		feed.registerCommand("feed", this.getName());
		new FlyCommand(this).registerCommand("fly", this.getName());
		new WorldCommand(this).registerCommand("world", this.getName());
	}

	private void setupNetworkPlugin()
	{
		this.networkPlugin = NetworkCoreAPI.getPluginManager().getPlugin(this);
	}

	private void validateNetworkCoreDependency()
	{
		PluginManager pm = this.getServer().getPluginManager();
		Plugin networkCore = pm.getPlugin("NetworkCore");
		if (networkCore == null) { throw new IllegalStateException("Missing NetworkCore depenency"); }
	}
}
