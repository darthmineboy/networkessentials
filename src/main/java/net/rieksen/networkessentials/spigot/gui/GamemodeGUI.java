package net.rieksen.networkessentials.spigot.gui;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import net.md_5.bungee.api.ChatColor;
import net.rieksen.networkcore.core.user.User;
import net.rieksen.networkcore.spigot.chestgui.BasicChestGUI;
import net.rieksen.networkcore.spigot.chestgui.ChestItem;
import net.rieksen.networkcore.spigot.chestgui.IChestItem;
import net.rieksen.networkessentials.spigot.NetworkEssentials;

public class GamemodeGUI extends BasicChestGUI
{

	private NetworkEssentials	plugin;
	private Player				player;

	/**
	 * @param plugin
	 * @param player
	 */
	public GamemodeGUI(NetworkEssentials plugin, Player player)
	{
		super(true);
		this.plugin = plugin;
		this.player = player;
	}

	@Override
	protected Inventory createInventory()
	{
		String title = this.plugin.getNetworkPlugin().getMessageSection("Gamemode Command").getMessage("gui-title")
			.getMessage(User.getUser(this.player));
		return this.plugin.getServer().createInventory(null, 9, ChatColor.translateAlternateColorCodes('&', title));
	}

	@Override
	protected Map<Integer, IChestItem> prepareContent(Player player)
	{
		Map<Integer, IChestItem> content = new HashMap<>();
		int slots[] = { 1, 3, 5, 7 };
		int i = 0;
		for (GameMode gamemode : GameMode.values())
		{
			int slot = slots[i];
			ItemStack item = this.buildItemStackOfGamemode(gamemode);
			content.put(slot, new ChestItem(item)
			{

				@Override
				public void onInventoryClick(InventoryClickEvent e)
				{
					player.setGameMode(gamemode);
					String message =
						GamemodeGUI.this.plugin.getNetworkPlugin().getMessageSection("Gamemode Command").getMessage("gamemode-changed")
							.getMessage(User.getUser(player)).replace("%gamemode%", gamemode.name().toLowerCase());
					player.sendMessage(ChatColor.translateAlternateColorCodes('&', message));
					player.closeInventory();
				}
			});
			i++;
		}
		return content;
	}

	private ItemStack buildItemStackOfGamemode(GameMode gamemode)
	{
		ItemStack item;
		switch (gamemode)
		{
			case ADVENTURE:
				item = new ItemStack(Material.COMPASS);
				break;
			case CREATIVE:
				item = new ItemStack(Material.SPONGE);
				break;
			case SPECTATOR:
				item = new ItemStack(Material.GLASS);
				break;
			case SURVIVAL:
				item = new ItemStack(Material.DIAMOND_SWORD);
				break;
			default:
				item = new ItemStack(Material.BEDROCK);
				break;
		}
		ItemMeta meta = item.getItemMeta();
		String displayName = this.plugin.getNetworkPlugin().getMessageSection("Gamemode Command").getMessage("gamemode-item-name")
			.getMessage(User.getUser(this.player)).replace("%gamemode%", gamemode.name().toLowerCase());
		meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', displayName));
		item.setItemMeta(meta);
		return item;
	}
}
